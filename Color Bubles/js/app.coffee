
layerStart = new Layer
	width: 200
	height: 200
	backgroundColor: "#EA5455"
layerStart.centerX()
layerStart.borderRadius = 100

layerLogo = new Layer
	 image: "images/logo.png"

layerPopUp = new Layer
	backgroundColor: "#fff"
layerPopUp.style =
	"width":"100vw",
	"height":"200px"
layerPopUp.states.add
	second:
			y:200
layerStart.on Events.Click,() ->
    layerPopUp.states.next("second")


