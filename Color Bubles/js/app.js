layerStart = new Layer({
  width: 200,
  height: 200,
  backgroundColor: "#EA5455"
});
layerStart.centerX();
layerStart.borderRadius = 100;
layerStart.y = 1000;


layerBall1 = new Layer({
  backgroundColor: "#53CDD8"
});
layerBall1.y = -200;
layerBall1.style = {
  "width": "200px",
  "height": "200px"
};
layerBall1.borderRadius = 100;
layerBall1.states.add({
	second: {y:300,x:500},
})
layerBall1.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerStart.on(Events.Click, function() {
	layerBall1.states.next()
})


layerBall2 = new Layer({
  backgroundColor: "#F07B3F"
});
layerBall2.y = -200;
layerBall2.x = 100;
layerBall2.style = {
  "width": "200px",
  "height": "200px"
};
layerBall2.borderRadius = 100;
layerBall2.states.add({
	second: {y:100,x:200},
})
layerBall2.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerBall1.on(Events.Click, function() {
	layerBall2.states.next()
})


layerBall3 = new Layer({
  backgroundColor: "#AA96DA"
});
layerBall3.y = -200;
layerBall3.x = 100;
layerBall3.style = {
  "width": "200px",
  "height": "200px"
};
layerBall3.borderRadius = 100;
layerBall3.states.add({
	second: {y:700,x:50},
})
layerBall3.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerBall2.on(Events.Click, function() {
	layerBall3.states.next()
})


layerBall4 = new Layer({
  backgroundColor: "#E9B5D2"
});
layerBall4.y = -200;
layerBall4.x = 800;
layerBall4.style = {
  "width": "200px",
  "height": "200px"
};
layerBall4.borderRadius = 100;
layerBall4.states.add({
	second: {y:100,x:50},
})
layerBall4.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerBall3.on(Events.Click, function() {
	layerBall4.states.next()
})


layerBall5 = new Layer({
  backgroundColor: "#FCE38A"
});
layerBall5.y = -200;
layerBall5.x = 800;
layerBall5.style = {
  "width": "200px",
  "height": "200px"
};
layerBall5.borderRadius = 100;
layerBall5.states.add({
	second: {y:700,x:500},
})
layerBall5.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerBall4.on(Events.Click, function() {
	layerBall5.states.next()
})


layerBall6 = new Layer({
  backgroundColor: "#63B75D"
});
layerBall6.y = -200;
layerBall6.x = 800;
layerBall6.style = {
  "width": "200px",
  "height": "200px"
};
layerBall6.borderRadius = 100;
layerBall6.states.add({
	second: {y:500,x:300},
})
layerBall6.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerBall5.on(Events.Click, function() {
	layerBall6.states.next()
})