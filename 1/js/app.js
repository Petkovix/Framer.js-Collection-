var layerPopUp, layerStart;

layerStart = new Layer({
  width: 600,
  height: 200,
  backgroundColor: "#EA5455"
});
layerStart.centerX();
layerStart.y = 1000;

layerPopUp = new Layer({
  backgroundColor: "#fff"

});
layerPopUp.y = -200;
layerPopUp.style = {
  "width": "100vw",
  "height": "200px"
};
layerPopUp.states.add({
	second: {y:0},
})
layerPopUp.states.animationOptions = {
	curve: "spring(200,12,0)"
}
layerStart.on(Events.Click, function() {
	layerPopUp.states.next()
})
